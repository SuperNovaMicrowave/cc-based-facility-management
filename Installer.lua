-- pastebin: 8Nv9d6YC

function download(url, file)
    local content = http.get(url).readAll()
    if not content then
        error("Could not connect to website")
    end
    f = fs.open(file, "w")
    f.write(content)
    f.close()
end

if fs.exists("/bfm") then
    fs.delete("/bfm")
end
fs.makeDir("/bfm")


download("https://gitlab.com/SuperNovaMicrowave/cc-based-facility-management/-/raw/master/Based-Facility-Dasboard.lua", "/bfm/Based-Facility-Dashboard.lua")


if fs.exists("/startup") then
    fs.delete("/startup")
end
startup_file = fs.open("/startup", "w")
startup_file.writeLine("shell.setDir(\"/bfm\")")
startup_file.write("shell.run(\"Based-Facility-Dashboard.lua\")")
startup_file.close()
