-- debug terminal setup
term.clear()
term.setCursorPos(1,1)
term.setTextColor(colors.blue)
term.write("Based Facility Management v69")
term.setCursorPos(1,2)


-- peripheral setup
local monitor = peripheral.find("monitor")
monitor.setTextScale(0.5)
local mon_width, mon_height = monitor.getSize()

local bundledRedstoneSide = "right"


-- runtime vars
local elevatorUnlocked = false
local currentPage = 1
local pageCount = 3


-- auxilliary functions
function coloredTextAt(device, x, y, background_color, foreground_color, text)
    device.setCursorPos(x, y)
    device.setBackgroundColor(background_color)
    device.setTextColor(foreground_color)
    device.write(text)
end

function pageTitle(page_title, page_number)
    
    -- title background
    _oldterm = term.redirect(monitor)
        paintutils.drawFilledBox(3, 1, mon_width - 2, 1, colors.lightGray)
    term.redirect(_oldterm)

    -- title background decoration
    coloredTextAt(monitor, 3, 2, colors.black, colors.lightGray, string.char(130))
    coloredTextAt(monitor, 4, 2, colors.black, colors.lightGray, repeatChar(string.char(131), mon_width - 6))
    coloredTextAt(monitor, mon_width - 2, 2, colors.black, colors.lightGray, string.char(129))

    -- title
    coloredTextAt(monitor, 4, 1, colors.lightGray, colors.blue, page_title)
    -- number
    coloredTextAt(monitor, mon_width-3, 1, colors.lightGray, colors.blue, page_number)
end

function repeatChar(char, count)
    out = ""
    while string.len(out) < count do
        out = out .. char
    end
    return out
end


-- thread functions
function pollInfoFunc()
    while true do

        local old_elevatorUnlocked = elevatorUnlocked
        elevatorUnlocked = redstone.testBundledInput(bundledRedstoneSide, colors.red)
        if old_elevatorUnlocked ~= elevatorUnlocked then
            local state = elevatorUnlocked and "unlocked" or "locked"
            print("[INFO] The Elevator is now " .. state .. ".")
        end

        os.sleep(.05) -- prevent "too long with out yielding"
    end
end

function pollTouchFunc()
    while true do

        local event, dev, x, y = os.pullEvent("monitor_touch")

        print("[INPUT] " ..
            "The device " .. dev .. " was touched at" ..
            " X: " .. tostring(x) ..
            " Y: " .. tostring(y))

        local old_currentPage = currentPage
        if x == 1 then
            currentPage = math.max(1, currentPage-1)
            initScreen()

            print("[DISPLAY] Switching from page " .. tostring(old_currentPage) .. " to " .. tostring(currentPage))
        elseif x == mon_width then
            currentPage = math.min(pageCount, currentPage+1)
            initScreen()

            print("[DISPLAY] Switching from page " .. tostring(old_currentPage) .. " to " .. tostring(currentPage))
        end

        os.sleep(.05) -- prevent "too long with out yielding"
    end
end

function drawScreenFunc()
    while true do

        -- page 1
        if currentPage == 1 then
            -- elevator status dynamic text
            if elevatorUnlocked then
                coloredTextAt(monitor, 5, 5, colors.red, colors.white, "UNLOCKED")
            else
                coloredTextAt(monitor, 5, 5, colors.green, colors.white, " LOCKED ")
            end

        -- page 2
        elseif currentPage == 2 then
            -- todo

        -- page 3
        elseif currentPage == 3 then
            -- todo
        end

        os.sleep(.05) -- prevent "too long with out yielding"
    end
end


-- miscellaneous functions
function initScreen()
    monitor.setBackgroundColor(colors.black)
    monitor.setTextColor(colors.white)
    monitor.clear()

    -- page switch buttons
    for y = 1, mon_height, 1 do
        coloredTextAt(monitor, 1, y, colors.white, colors.black, "<")
        coloredTextAt(monitor, mon_width, y, colors.white, colors.black, ">")
    end

    -- page 1 (status)
    if currentPage == 1 then
        pageTitle("FACILITY STATUS", "1")

        -- elevator status box
        coloredTextAt(monitor, 4, 3, colors.gray, colors.black, repeatChar(string.char(131), 10))
        coloredTextAt(monitor, 4, 4, colors.gray, colors.black, repeatChar(" ", 10))
        coloredTextAt(monitor, 4, 5, colors.gray, colors.black, repeatChar(" ", 10))
        coloredTextAt(monitor, 4, 6, colors.black, colors.gray, repeatChar(string.char(143), 10))
        -- rounded corners
        coloredTextAt(monitor, 4, 3, colors.gray, colors.black, string.char(135))
        coloredTextAt(monitor, 13, 3, colors.gray, colors.black, string.char(139))
        coloredTextAt(monitor, 4, 6, colors.black, colors.gray, string.char(139))
        coloredTextAt(monitor, 13, 6, colors.black, colors.gray, string.char(135))

        -- elevator status static text
        coloredTextAt(monitor, 5, 4, colors.gray, colors.white, "ELEVATOR")

    -- page 2 (map)
    elseif currentPage == 2 then
        pageTitle("DUMMY PAGE", "2")
        -- todo

    -- page 3 (dummy)
    elseif currentPage == 3 then
        pageTitle("DUMMY PAGE", "3")
        -- todo
    end
end


-- manually initialize screen for the first time
initScreen()

-- execute threads
parallel.waitForAll(pollInfoFunc, pollTouchFunc, drawScreenFunc)
